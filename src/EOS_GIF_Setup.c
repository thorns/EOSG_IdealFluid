#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "util_String.h"
#include "util_ErrorCodes.h"
#include "util_Table.h"

#include "EOS_GIF.h"

void EOS_GIF_Startup (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr, table_handle;
  
  table_handle = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
  
  Util_TableSetInt(table_handle,
                   N_INDEPS,
                   "N independent variables");
  Util_TableSetInt(table_handle,
                   N_DEPS,
                   "N dependent variables");
  Util_TableSetString(table_handle,
                      "Rho SpecificInternalEnergy",
                      "Independent variable names");
  Util_TableSetString(table_handle,
                      "Pressure DPressureDRho DPressureDSpecificInternalEnergy c_s^2 Temperature",
                      "Dependent variable names");
  Util_TableSetString(table_handle,
                      "Ideal Fluid",
                      "EOS Name");

  ierr = EOS_RegisterCall(table_handle,
                          EOS_GIF_SetArray);
  if (ierr)
  {
    CCTK_WARN(0, "Failed to set up EOS_IdealFluid call");
  }
}

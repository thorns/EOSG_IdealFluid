 /*@@
   @file      EOS_GIF_Setup.F90
   @date      Mon Mar 14 17:41:20 2005
   @author    Ian Hawke
   @desc 
   Setup the scalar variables
   @enddesc 
 @@*/

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

subroutine EOS_GIF_Setup()

  USE EOS_GIF_Scalars

  implicit none

  DECLARE_CCTK_PARAMETERS

  eos_if_gamma_local = eos_ideal_fluid_gamma
  mean_molecular_weight_local = mean_molecular_weight
  k_boltzmann = 1.d80
  
end subroutine EOS_GIF_Setup
